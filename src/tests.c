#include "tests.h"
#include "util.h"

int free_heap(void* heap, size_t length) {
    return munmap(heap, size_from_capacity((block_capacity) { .bytes = length }).bytes);
}

void* start(size_t heap_size) {
    void* heap = heap_init(heap_size);
    debug_heap(stdout, heap);
    return heap;
}

void end(void* heap, size_t heap_size) {
    free_heap(heap, heap_size);
}


void test1() {
    println("Test simple allocation");
    void* block = _malloc(10);
    if (!block) {
        println_err("Test 1 FAILED");
        return;
    }
    _free(block);
    println("Test 1 PASSED");
}

void test2() {
    println("Test free one block from many");
    void* block1 = _malloc(10);
    void* block2 = _malloc(20);
    if (!block1 || !block2) {
        println_err("Test 2 FAILED");
        return;
    }
    _free(block1);
    _free(block2);
    println("Test 2 PASSED");

}

void test3() {
    println("Test free two blocks from many");
    void* block1 = _malloc(10);
    void* block2 = _malloc(20);
    void* block3 = _malloc(30);
    if (!block1 || !block2 || !block3) {
        println_err("Test 3 FAILED");
        return;
    }
    _free(block1);
    _free(block2);
    _free(block3);
    println("Test 3 PASSED");

}

void test4(size_t heap_size) {
    println("Test new region");
    void* block = _malloc(heap_size + 1);
    if (!block) {
        println_err("Test 4 FAILED");
        return;
    }
    _free(block);
    println("Test 4 PASSED");
}

void test5(size_t heap_size) {
    println("Test new region in other place");
    (void) mmap(HEAP_START + REGION_MIN_SIZE, REGION_MIN_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED, -1, 0);
    void* block = _malloc(heap_size + 1);
    if (!block) {
        println_err("Test 5 FAILED");
        return;
    }
    _free(block);
    println("Test 5 PASSED");
}