#include "tests.h"

int main() {
	size_t heap_size = REGION_MIN_SIZE;
	void* heap = start(heap_size);
	if (!heap) {
		printf("Failed to initialize heap");
		return 0;
	}
	test1();
	test2();
	test3();
	test4(heap_size);
	test5(heap_size);
	end(heap, heap_size);
	return 0;
}