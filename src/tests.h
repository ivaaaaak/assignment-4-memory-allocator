#ifndef TEST_H
#define TEST_H

#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <sys/mman.h>

void* start(size_t heap_size);

void test1();

void test2();

void test3();

void test4(size_t heap_size);

void test5(size_t heap_size);

void end(void* heap, size_t heap_size);

#endif